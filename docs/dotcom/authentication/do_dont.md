## Do's and Don'ts

As we hammer out the full authentication procedures/policies, this article may transform to full on policy guidelines. For the time being, please consider these things when dealing with customers.

1. **NEVER** provide any information that is not publicly viewable about a user to another user.
    - This includes things like email addresses. Navigate to the user's profile to see if that is made public - if not, never provide this under any circumstances. 
    - Do not even confirm or deny whether a user or their private group/project/namespace exists. Let customers know that 404 could mean that it does not exist or that it could be private. Even if it is clearly nonexistant, confirming that is an avenue that will allow social hackers to gain information we should not provide.
    - Users may contact us using another account and claim that a particular username belongs to them, but they have forgotten the password. Unless this can be proven, assume that they are separate users and follow the proper procedures.
    - Remember that it is very possible for two people to have the same name - just because their first/last names match, does not mean you should think the person submitting the ticket and the user account are the same person. Follow the proper authentication methods.
    - On GitLab.com, each user is an indivdual customer - if a group owner contacts us regarding one of the users in that group, they are **not** entitled to information about that user. If needed, please have them CC the other user, authenticate, then ask for permission to provide the owner with information as needed. Otherwise, have the other user submit a separate ticket.

2. **DO** check a user's permission level of the group.
    - If a user does not have Master/Owner permission of the group they are asking about, follow similar steps as above when providing information.
    - Impersonate as the user to double-check what that user can see/do before providing support.

3. **DO NOT** edit or take action on a customer's account whenever possible.
    - Instead, guide the customers to do what they need on their own. 
    - If all troubleshooting steps have been taken, and you need to test directly, get explicit, written permission from the customer before making any changes to their account.
    - When in doubt, ask a SaaS Support Engineer or Production Engineer. 

4. **DO** be wary about your answers.
    - It is possible to inadvertently break authentication protocol with the language you use.
    - When necessary, do not speak in absolutes. For example - on 404's, state that the namespace _may_ be private.
    - Be careful with any screenshots you provide. Ensure there is nothing on the screenshot that the customer would not have access to. 
