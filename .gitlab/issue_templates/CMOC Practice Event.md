
<!-- Coordinator of quarterly event creates new issue 
- Set title to "CMOC Practice event FY<YY>-Q<q>
- Update heading with YY and q
- Create a new feedback document based on the template (link tbd - JG sorting out rights to support shared drive as at 26 Jan 2021)
- Link above feedbac document below in References section
- Coordinate with IMOC to 
   - determine dates of events (can do 1 at time, any time during the quarter), populate feedback doc and schedule events with IMOC & notetaker
   - agree on script to use (can be changed for each event depending on experience of CMOC rostered - make a note in the feedback doc of which script used per region)
-->
## CMOC Practice event FY<YY>-Q<q>

CMOC practice events will occur in all 3 regions for the quarter. The exact date and time of each event will not be known to the CMOC ahead of time.

### References 

- [Practice Event Feedback document - FY<yy>-Q<q>]()
- [Script Library](https://docs.google.com/document/d/1vz64G0KUVnDwyUbqNsk12VuakU_IkeEfZtBBGFNrLtM/edit#heading=h.1acgctaxu9mf) - confidential to IMOC and note taker
- [CMOC Practice Events](link to hb page about the events) process details

### Event Runbook

**Initiate Practice Incident**

1. IMOC: [initiate](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#report-an-incident-via-slack) the practice incident by using `/incident declare` in [#production](https://gitlab.slack.com/archives/C101F3796)   
  - set severity as `S4`
  - set title as `TEST: CMOC practice event` - this ensures the SRE team know they don't need to respond   
  - ONLY tick to page the `Communications Manager on Call`
2. IMOC & Notetaker: join the zoom room linked in [#incident_management](https://gitlab.slack.com/archives/CB7P5CJS1) - NOTE this is the default incident zoom room and we will vacate it shortly to ensure it's available for any live events. The following will be done by the incident app:
   - CMOC  paged
   - incident auto-created for the event
   - Slack channel auto-created for the event - use this for any discussion during the practice event
3. CMOC joins the incident Zoom room

**Introduction to event & move Zoom room**
*IMOC*

1. Briefly explain this is a practice event
2. Create a new Zoom room and share a link to it in the event's Slack channel - this frees up the Incident Management zoom room for live events
3. Update the topic description of the practice event Slack channel to remove the default incident zoom link and add the one shared above (if others join the event this will allow them to join the correct Zoom room)

*All Participants*

1. Vacate the incident room and join the Zoom shared above

**Practice Event Begins **

1. IMOC starts recording on Zoom
2. IMOC explains the purpose of the event and begins the scenario
3. CMOC logs into status.io, using the test page and posts updates as appropriate to the scenario  -remember to refer to the CMOC [Handbook](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html) pages!
4. IMOC to paste screenshots of test tweets to incident slack channel for reference

**Discussion points**

1. How to [escalate action items](https://about.gitlab.com/handbook/engineering/development/processes/Infra-Dev-Escalation/process.html) to infradev
2. Was this helpful?
3. Was the documentation clear?

### Post practice event actions

Notetaker and IMOC to agree on DRI for the following:

1. Note any improvements/corrections/feedback in the practice event feedback doc
2. Update template documents and handbook as necessary
