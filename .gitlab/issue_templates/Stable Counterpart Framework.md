Thank you for being part of the first iteration of the Support Stable Counterpart framework. This template was created by using the feedback that was gathered in our previous calls. The goal here is to ensure that:

* We limit the barriers that may affect whether a team-member can join the stage of their choice  
* The responsibilities of the role are clear so as not to overwhelm the team-member and empower them to recognize what may be out of the role’s scope  
* The support team and product group are able to mutually benefit   

### Select the type of call you will be joining, as well as the frequency 

* Create a [Support Stable Counterpart Meeting issue](.gitlab/issue_templates/stable_counterparts/Support Stable Counterpart Call.md) so we can track the progress of our framework and have a better idea of the changes to bring in the next iteration.

### This issue can be closed once the following tasks have been completed: 

* [ ] You have joined a team meeting or had a 1:1 with the Product Manager   
* [ ] You’ve submitted a Support Fix(this will help us gather feedback on the time it took from review to merge)
* [ ] You have shared release updates with the Support team 
* [ ] You have gathered enough feedback to contribute to our next call and iteration  

/label ~support-stable-counterpart-framework

/milestone  %Revamp Support Stable Counterpart Role

/epic &57
