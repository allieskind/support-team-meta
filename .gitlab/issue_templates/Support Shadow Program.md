**Title: Support Shadow Program - Your Name**

This issue is to help organize and track progress during your participation in the GitLab Support Shadow Program.

The Support Shadow Program's first iteration is "Support for a half-day".

Team members outside the Support team volunteer to spend half of one workday shadowing and working with Support team members.

1. 60-minute prep work in advance of the Shadow (at your leisure).
1. 200 minutes of Shadowing during one workday (divided between four 50-minute Zoom calls).
1. Short survey sharing feedback on your experience at the end.

## Stage 1: Communication and Planning

### For Support Shadow (Participant)

- [ ] Discuss your participation in the Support Shadow Program with your manager and ping them on this issue.
- [ ] Pick a day when you'll be available for calls shadowing and working alongside Support Engineers.
- [ ] Announce that you'll be participating in `#support_team-chat` Slack and Support Week in Review *at least one week in advance* of your start date.
- [ ] (Optional) Think of a deliverable or end result that you could create to share what you've learned with your team members.

<!-- PARTICIPANT will be the Support Shadow Program participant. -->

/assign me

### For Support Team (Hosts)

- [ ] Request for volunteers to host a 50-minute Support Shadow session in Support Week in Review and Slack one week prior to the Shadow Program.
  - [ ] Volunteer for GitLab Self-Managed Support pairing: ______
  - [ ] Volunteer for GitLab SaaS Support pairing: ______
  - [ ] Volunteer for Third Pairing Session Volunteer: ______
- [ ] Have each volunteer schedule a 50-minute meeting with the participant on the day.
- [ ] Talk to @greg about scheduling a "Support Shadow Q&A" meeting on the Support Team Calendar for the day of the Support Shadow Program.

<!-- SUPPORT_HOST_DRI (@greg for the interim) will be DRI organizing the Support Team "Hosting" of Shadow and Q&A/AMA meetings.-->

/assign @greg

## Stage 2: Before you start

### Get to know the GitLab Support team

#### Who we are

The GitLab Support team is a large globally distributed team of helpful, smart, and kind individuals.

For an overview of the Support team members you'll be working with during the Shadow Program and beyond, check the ["Meet Our Team" page for Support](https://about.gitlab.com/company/team/?department=customer-support).

#### Slack

Join the following Slack channels:

- `#support_self-managed`
- `#support_gitlab-com`
- `#support_escalations`
- `#support_team-chat` (optional)

### Support Handbook, Workflows, and Training

Support Shadow Participants should do up to 1-hour of "prep work" of reading the GitLab Support handbook, workflows, job duties, roles, and training materials.

Familiarizing yourself with this content will provide context that can improve the Support Shadow Program experience. You are *not* expected to read or remember everything.

For the best experience, we suggest Support Shadow Program participants review the following before Shadowing:

#### Support Handbook & Workflows

- [ ] [Support Page](https://about.gitlab.com/support/)
- [ ] [Support Handbook](https://about.gitlab.com/handbook/support/)
- [ ] [Support Workflows](https://about.gitlab.com/handbook/support/workflows/)

#### Support Training

- [ ] [Support Training Handbook](https://about.gitlab.com/handbook/support/training/)
- [ ] [Onboarding](https://about.gitlab.com/handbook/support/training/#support-engineer-onboarding-pathway)
- [ ] [Training Issues and Modules](https://gitlab.com/gitlab-com/support/support-training/-/tree/master/.gitlab/issue_templates)
- [ ] [Git & GitLab Basics](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Git-GitLab-Basics.md)
- [ ] [GitLab Installation & Administration Basics](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/GitLab-Installation-Administration-Basics.md)
- [ ] [Gitlab Support Basics](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/GitLab-Support-Basics.md)
- [ ] [Working on Tickets](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Working-On-Tickets.md)
- [ ] [GitLab.com Basics](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/GitLab-com-Basics.md)
- [ ] [Self-managed Basics](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Self-Managed-Basics.md)
- [ ] [License and Renewals](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/License%20and%20Renewals.md)

## Stage 3: Shadowing and working alongside Support

Support Shadows are expected to participate in three 50-minute calls shadowing Support team members.

There will also be a Support AMA/Q&A session for the Support Shadow.

- [ ] Shadow 1 - Self-Managed
- [ ] Shadow 2 - SaaS
- [ ] Shadow 3 - Additional Pairing (your choice: SaaS, Self-managed, L&R, or Manager)
- [ ] Support Team Shadow Q&A

## Stage 4 (*optional*)

Stage 4 is an *optional* extension of the Support Shadow Program experience.

Options include attending Support team meetings and Technical Support challenges.

### Shadow a GitLab Support Team meeting

Each region holds a weekly regional Support Team meeting.

Support Shadow Program Participants are invited to attend and "shadow" Support Team meetings during their Shadow week (see GitLab Team Meetings calendar for meeting times).

### Technical Support Challenge of your choosing

To participate in an optional **technical challenges**, mark the checkbox next to the challenge of your choosing.

#### Basic Challenges

These hands-on challenges may require access to virtual machine resources or account access requests.

If you'd like to participate in these challenges, notify your Support Host (`@greg`) at least *one week in advance*.

- [ ] 1. Self-managed fundamentals

   1. [Install GitLab EE](https://about.gitlab.com/install/).
   2. [Install GitLab Runner](https://docs.gitlab.com/runner/install/).
   3. [Register GitLab Runner with your GitLab EE instance](https://docs.gitlab.com/runner/register/).

- [ ] 1. SaaS Fundamentals

   1. Find your own GitLab user's activity in [Kibana](https://about.gitlab.com/handbook/support/workflows/kibana.html).
   2. Find logs in [Kibana](https://about.gitlab.com/handbook/support/workflows/kibana.html) for a 500 error on GitLab.com.

#### Advanced Support Challenges

Advanced challenges are an exercise in using GitLab documentation and knowledge of the GitLab product to find answers to customer questions.

- [ ] Verbally walk through the upgrade path and process required to update GitLab Omnibus from version 11.9.x to the latest release.
- [ ] Verbally walk through steps to migrate GitLab data from one GitLab SM instance to another, from SM to SaaS, and from SaaS to SM.
- [ ] Verbally walk through the steps to set up a 2k+ [reference architecture](https://docs.gitlab.com/ee/administration/reference_architectures/#available-reference-architectures).

## Stage 5: Share Feedback and Suggestions to help improve Future Iterations

- [ ] Fill out the following survey in the issue comments to provide feedback on your experience: what was valuable, and what could be improved?

```markdown
Support Shadow Program Participant Survey

1. What interested in the Support Shadow Program?
1. Would you recommend that others in your role participate in the Support Shadow Program?
1. On a scale of 1-10, how would you rate your Support Shadow Program Experience?
1. What did you most helpful or valuable about the Support Shadow Program?
1. What did you find least helpful or valuable during your Support Shadow Program?
1. Do you have suggestions or ideas on how this program could be improved in future iterations?
```
