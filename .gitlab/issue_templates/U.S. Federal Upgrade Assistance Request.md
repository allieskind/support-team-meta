**Please use this as the Title**: Upgrade Assistance Request - U.S. Federal Customer

Reminder: **Do NOT put any customer confidential info in this issue**. Gitlab.com issues, even if made confidential, may be accessed by non-US Citizen GitLab Team members.

### Overview:

#### Required Information

| | |
| -- | -- |
|  **Link to account in SFDC/ZD** |  [US Federal Customer](https://gitlab-federal-support.zendesk.com/agent/search/)   |
|  **Link to SFDC/ZD Point of contact user:** | [US Federal User](https://gitlab-federal-support.zendesk.com/agent/search/)   |
|  **Ticket link (if applicable):** |  [Ticket #](https://gitlab-federal-support.zendesk.com/agent/search/)   |
|  **Timezone:** |    |
|  **Requested dates / times:** |     |
|  **Upgrade plan (link to confidential doc):** |      |
|  **Rollback plan (link to confidential doc):** |     |
|  **Architecture documentation (link to confidential doc):** |    |
|  **Additional relevant information (optional):** |     |

___

### Task Checklists

#### TAM Tasks

  * [ ] Supply above Required Information
  * [ ] Ensure the proper amount of advanced notice has been given:
    * One week if the upgrade window is during [federal support hours](https://about.gitlab.com/support/#hours-of-operation)
    * Two weeks if the upgrade window is outside [federal support hours](https://about.gitlab.com/support/#hours-of-operation)

#### Support Managers Tasks

  * [ ] Ensure this is assigned this to yourself and the AMER Support Manager who will be on-call during the upgrade window
	- Note: not all AMER managers have access to U.S. Federal Instance. Please loop in one that does
  * [ ] AMER manager w/Fed Access to review required information was provided and that confidential issue is not shared publically in this issue
  * [ ] Select an engineer to perform the Pre-Scheduling Tasks and assign them to this issue (within 24 hours of issue creation)
  * [ ] Select an engineer to do the Pre-call and Post-call Tasks and assign them to this issue
  * [ ] Inform the [support engineer(s) on-call](https://gitlab.pagerduty.com/schedules#P89ZYHZ) during the upgrade window so that they are aware of a potential emergency source

#### Support Engineer Tasks
##### Support Engineer Pre-Scheduling Tasks
  * [ ] Review user's upgrade plan and architecture documentation
  * [ ] Create a ticket reaching out to the user to reach out (if one does not already exist)
    - [ ] Ensure the OS package manager can reach the package repositories
    - [ ] Ensure backups/snapshots are taken prior to the scheduled call
    - [ ] Recommend disabling antivirus to help speed up the process when issuing upgrade commands
  * [ ] Comment below with the Zendesk ticket link
  * [ ] Update Zendesk ticket to contain this issue's link
  * [ ] Comment below once upgrade plan and architecture documentation are in a good state and we are ready to schedule the call

##### Support Engineer Pre-call Tasks
  * [ ] Schedule a call for the requested time frame for the user at least 24 hours prior to the upgrade window (ensure a Google calendar event containing a Zoom link is made)
  * [ ] Send the user the information about the call via the Zendesk ticket
  * [ ] Comment below confirm the scheduling of the call

##### Support Engineer Post-call Tasks
  * [ ] Comment below with any post-call notes (did it go well, did something break, was a rollback needed, etc.)
  * [ ] Close the issue

___

This issue is assigned to Support Managers and next steps will be determined by them. If no actions related to this issue happen within 24h, share this issue in `#support_managers` Slack channel.
In case this is an [emergency](https://about.gitlab.com/support/#definitions-of-support-impact), please make the customer open an emergency ticket as this issue has no SLA associated with it.

Be aware that the support engineer will only be expected to join for the first
30 minutes of the upgrade. It is up to the support engineer's discretion if they
want to stay on beyond 30 minutes.

Please review the [Upgrade Assistance Page](https://about.gitlab.com/support/scheduling-live-upgrade-assistance.html) for more details.



/label ~"upgrade-assistance"
/label ~"customer-call"
/assign @gitlab-com/support/managers

/confidential

