Title: Support Stable Counterpart - Date 

## Call type:

* [ ] Join retro calls to be aware of the Merge Requests that made it to the next release 
* [ ] Schedule weekly/bi-weekly/monthly call with the Product Manager to be kept in the loop   
* [ ] Monthly when the team plans the next Milestone  

## Notes

- Topics discussed:
 1.
 1.
 1.

- New features to track:
 1.
 1.
 1.

/label ~support-stable-counterpart-call

/milestone  %"Revamp Support Stable Counterpart Role"

/epic &57

