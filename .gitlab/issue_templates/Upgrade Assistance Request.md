Title: Upgrade Assistance Request - [Fill in company name / domain]

### Overview:

#### Required Information

| | |
| -- | -- |
|  **Customer:** |     |
|  **Point of contact for customer (email address preferred):** |     |
|  **Ticket (if applicable):** |     |
|  **Requested dates / times in UTC:** |     |
|  **Upgrade plan (attachment or link):** |     |
|  **Rollback plan (attachment or link):** |     |
|  **Architecture documentation (attachment or link):** |     |
|  **Current GitLab Version:** |     |
|  **Target GitLab Version:** |     |
|  **Additional relevant information (optional):** |     |

___

### Task Checklists

#### TAM Tasks

  * [ ] Supply above Required Information
  * [ ] Ensure the proper amount of advanced notice has been given:
    * One week if the upgrade window is during regular business hours
    * Two weeks if the upgrade window is during non-business hours (weekends, holidays, etc.)

#### Support Managers Tasks

  * [ ] Assign this issue to yourself and to the Support Manager who will be on-call during the upgrade window
  * [ ] Review required information was provided
  * [ ] Select an engineer to perform the Pre-Scheduling Tasks and assign them to this issue (within 24 hours of issue creation)
  * [ ] Select an engineer to do the Pre-call and Post-call Tasks and assign them to this issue
  * [ ] Inform the support engineer(s) on-call during the upgrade window so that they are aware of a potential emergency source

#### Support Engineer Tasks
##### Support Engineer Pre-Scheduling Tasks
  * [ ] Review user's upgrade plan and architecture documentation
  * [ ] Create a ticket reaching out to the user to reach out (if one does not already exist)
  * [ ] Comment below with the Zendesk ticket link
  * [ ] Update Zendesk ticket to contain this issue's link
  * [ ] Comment below once upgrade plan and architecture documentation are in a good state and we are ready to schedule the call

##### Support Engineer Pre-call Tasks
  * [ ] Schedule a call for the requested timeframe for the user at least one full business day prior to the upgrade window (ensure a Google calendar event containing a Zoom link is made)
  * [ ] Send the user the information about the call via the Zendesk ticket
  * [ ] Comment below confirm the scheduling of the call

##### Support Engineer Post-call Tasks
  * [ ] Comment below with any post-call notes (did it go well, did something break, was a rollback needed, etc.)
  * [ ] Close the issue

___

This issue is assigned to Support Managers and next steps will be determined by them. If no actions related to this issue happen within 24h, share this issue in `#support_escalations` Slack channel.
In case this is an [emergency](https://about.gitlab.com/support/#definitions-of-support-impact), please make the customer open an emergency ticket as this issue has no SLA associated with it.

Be aware that the support engineer will only be expected to join for the first
30 minutes of the upgrade. It is up to the support engineer's discretion if they
want to stay on beyond 30 minutes.

Please review the [Upgrade Assistance Page](https://about.gitlab.com/support/scheduling-live-upgrade-assistance.html) and [Live Upgrade Assistance workflow](https://about.gitlab.com/handbook/support/workflows/live-upgrade-assistance.html) for more details.

/label ~"upgrade-assistance"
/label ~"customer-call"
/assign @gitlab-com/support/managers 

/confidential
